<?php

declare(strict_types=1);

namespace App\Handler;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use MailchimpMarketing\ApiClient;


class WebhookHandlerFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        return new WebhookHandler($container->get(ApiClient::class));
    }
}
