<?php

declare(strict_types=1);

namespace App\Handler;

use AmoCRM\Client\AmoCRMApiClient;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use MailchimpMarketing\ApiClient;
use MailchimpMarketing\ApiException;


class WebhookHandler implements RequestHandlerInterface
{
    private const LIST_ID = '0672401718';
    private ApiClient $mailchimp;

    public function __construct(ApiClient $mailchimp)
    {
        $this->mailchimp = $mailchimp;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $customFields = $request->getParsedBody()['contacts']['update'][0]['custom_fields'];

        foreach ($customFields as $field) {
            if ($field['code'] === 'EMAIL') {
                $email = $field['values'][0]['value'];
                $name = $request->getParsedBody()['contacts']['update'][0]['name'];
                $pattern = '/\w+@\w+\.\w+/';

                try {
                    if (preg_match($pattern, $email)) {
                        $response = $this->mailchimp->lists->setListMember(
                            self::LIST_ID,
                            md5($email),
                            [
                                "email_address" => $email,
                                "status_if_new" => "subscribed",
                                "status" => "subscribed",
                                "merge_fields" => [
                                    "FNAME" => $name,
                                ]
                            ]
                        );
                    }
                } catch (ApiException $e) {
                    $response = new JsonResponse(['msg' => $e->getMessage()], 400);
                }
            }
            $response = new JsonResponse(['msg' => 'update'], 200);
        }

        return $response;
    }
}
