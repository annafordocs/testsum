<?php

declare(strict_types=1);

namespace App\Handler;

use AmoCRM\Client\AmoCRMApiClient;
use App\Workers\Executers\AccountSyncWorker;
use App\Workers\Model\Beanstalk;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use \http\Exception;
use \App\Models\User;


class AuthHandler implements RequestHandlerInterface
{
    public AmoCRMApiClient $client;
    protected Beanstalk $beanstalk;

    public function __construct(AmoCRMApiClient $client, Beanstalk $beanstalk)
    {
        $this->beanstalk = $beanstalk;
        $this->client = $client;
    }

    /**
     * @throws \AmoCRM\Exceptions\AmoCRMoAuthApiException
     * @throws \AmoCRM\Exceptions\AmoCRMMissedTokenException
     * @throws \AmoCRM\Exceptions\AmoCRMApiException
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $referer = $request->getQueryParams()["referer"];
        $code = $request->getQueryParams()['code'];


        if (isset($referer)) {
            $this->client->setAccountBaseDomain($referer);
        }

        try {
            $accessToken = $this->client->getOAuthClient()->getAccessTokenByCode($code);
            $accountId = $this->client->setAccessToken($accessToken)->account()->getCurrent()->getId();

            if (!$accessToken->hasExpired()) {
                $result = $this->saveToken(
                    [
                        'accessToken' => $accessToken->getToken(),
                        'refreshToken' => $accessToken->getRefreshToken(),
                        'expires' => $accessToken->getExpires(),
                        'baseDomain' => $this->client->getAccountBaseDomain(),
                        'amocrm_account_id' => $accountId,

                    ]

                );
            } else {
                $result = new JsonResponse(['error' => 'expired token'], 400);
            }
        } catch (Exception $exception) {
            $result = new JsonResponse(['error' => $exception->getMessage()], 400);
        }

        $domen = $this->client->getAccountBaseDomain();

        $this->beanstalk->send(AccountSyncWorker::QUEUE, ['baseDomain' => $domen]);

        $result = new JsonResponse(['test' => $domen]);
        return $result;
    }

    public function saveToken(array $accessToken): JsonResponse
    {
        try {
            if (isset($accessToken['accessToken'])
                && isset($accessToken['refreshToken'])
                && isset($accessToken['expires'])
                && isset($accessToken['baseDomain'])
            ) {
                $user = (new User($accessToken));

                /** @var $user User */
                $user->saveOrFail();
            }
        } catch (\Throwable $exception) {
            return new JsonResponse(['error' => $exception->getMessage()], 400);
        }

        return new JsonResponse(['msg' => 'token saved'], 200);
    }


}
