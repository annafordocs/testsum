<?php

declare(strict_types=1);

namespace App\Handler;

use AmoCRM\Client\AmoCRMApiClient;
use App\Workers\Model\Beanstalk;
use Psr\Container\ContainerInterface;
class AuthHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new AuthHandler($container->get(AmoCRMApiClient::class), $container->get(Beanstalk::class));

    }
}
