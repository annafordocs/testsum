<?php


namespace App\Handler;

use App\Workers\Model\Beanstalk;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;


class SumHandlerFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): SumHandler
    {
        return new SumHandler($container->get(Beanstalk::class));
    }
}
