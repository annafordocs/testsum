<?php


namespace App\Handler;

use MailchimpMarketing\ApiClient;
use Laminas\ServiceManager\Factory\FactoryInterface;


class MailchimpApiClientFactory implements FactoryInterface
{

    public function __invoke(\Interop\Container\ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $config = $container->get('config');

        $mailchimp = new ApiClient();

        $mailchimp->setConfig(
            [
                'apiKey' => $config['mailchimp']['APIkey'],
                'server' => $config['mailchimp']['prefix'],
            ]
        );

        return $mailchimp;
    }
}
