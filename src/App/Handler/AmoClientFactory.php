<?php

declare(strict_types=1);

namespace App\Handler;

use AmoCRM\Client\AmoCRMApiClient;
use Laminas\ServiceManager\Factory\FactoryInterface;

class AmoClientFactory implements FactoryInterface
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(\Interop\Container\ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $config = $container->get('config');

        return new AmoCRMApiClient(
            $config['oAuth']['clientId'],
            $config['oAuth']['clientSecret'],
            $config['oAuth']['redirectUri']
        );
    }
}
