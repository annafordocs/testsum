<?php

declare(strict_types=1);

namespace App\Handler;

use App\Workers\Model\Beanstalk;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;


class SumHandler implements RequestHandlerInterface
{
    protected Beanstalk $beanstalk;

    public function __construct(Beanstalk $beanstalk)
    {
        $this->beanstalk = $beanstalk;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $value1 = $request->getAttribute('val1');
        $value2 = $request->getAttribute('val2');

        return new JsonResponse(['sum' => $value1 + $value2]);
    }
}
