<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountContact extends Model
{
    protected string $table = 'account_contacts';

    protected array $fillable = [
        'name',
        'email',
        'amocrm_account_id',
    ];
}
