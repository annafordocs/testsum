<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */

    protected string $table = 'users';

    protected array $fillable = [
        'amocrm_account_id',
        'accessToken',
        'refreshToken',
        'baseDomain',
        'mailchimp_account_id',
        'expires',
    ];

}
