<?php

declare(strict_types=1);

namespace App\Workers\Executers;

use AmoCRM\Client\AmoCRMApiClient;
use App\Models\AccountContact;
use App\Models\User;
use App\Workers\Model\Beanstalk;
use League\OAuth2\Client\Token\AccessToken;
use MailchimpMarketing\ApiClient;
use MailchimpMarketing\ApiException;


class AccountSyncWorker extends BeanstalkWorker
{
    public const NAME = 'crm:worker:account_sync';
    public const QUEUE = 'account_sync';
    private const LIST_ID = '0672401718';

    protected AmoCRMApiClient $client;
    protected ApiClient $mailchimp;

    public function __construct(Beanstalk $queue, AmoCRMApiClient $client, ApiClient $mailchimp)
    {
        parent::__construct($queue);
        $this->client = $client;
        $this->mailchimp = $mailchimp;
    }

    protected function myName(): string
    {
        return self::NAME;
    }

    /**
     * @throws \AmoCRM\Exceptions\AmoCRMApiException
     * @throws \Throwable
     * @throws \AmoCRM\Exceptions\AmoCRMMissedTokenException
     * @throws \AmoCRM\Exceptions\AmoCRMoAuthApiException
     */
    protected function process($job): void
    {
        $domen = $job->baseDomain;

        $user = User::query()->where("baseDomain", "=", $domen)->first();

        $token = new AccessToken(
            [
                'access_token' => $user['accessToken'],
                'refresh_token' => $user['refreshToken'],
                'expires' => $user['expires'],
                'baseDomain' => $user['baseDomain'],
            ]
        );

        if ($token->hasExpired()) {
            $newAccessToken =
                $this->client
                    ->getOAuthClient()
                    ->getAccessTokenByRefreshToken($token);

            $user->setAttribute('access_token', $newAccessToken->getToken());
            $user->setAttribute('refresh_token', $newAccessToken->getRefreshToken());
            $user->setAttribute('expires', $newAccessToken->getExpires());
            $user->update();

            $token = $newAccessToken;
        }

        $contactsService = $this->client
            ->setAccessToken($token)
            ->contacts();

        $contacts = $contactsService->get();
        $this->saveContacts($contacts, $user);
    }


    public function saveContacts($contacts, $user)
    {
        foreach ($contacts as $contact) {
            if (!is_null($contact->getCustomFieldsValues())
                && !empty($contact->getCustomFieldsValues()->getBy('fieldCode', 'EMAIL'))) {
                $emailField = $contact->getCustomFieldsValues()->getBy('fieldCode', 'EMAIL')->getValues();

                foreach ($emailField as $item) {
                    $fields['name'] = $contact->getName();
                    $fields['email'] = $item->getValue();
                    $fields['amocrm_account_id'] = $user['amocrm_account_id'];

                    $addContact = (new AccountContact($fields));
                    $addContact->save();

                    $this->addToMailChimp($contact->getName(), $item->getValue());
                }
            }
        }
    }

    public function addToMailChimp($name, $email, $status = 'subscribed')
    {
        try {
            $response = $this->mailchimp->lists->addListMember(
                self::LIST_ID,
                [
                    "email_address" => $email,
                    "status" => $status,
                    "merge_fields" => [
                        "FNAME" => $name,
                    ]
                ]
            );
            print_r($response);
        } catch (ApiException $e) {
            echo $e->getMessage();
        }
    }

}
