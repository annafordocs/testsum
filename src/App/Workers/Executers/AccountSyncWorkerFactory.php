<?php


namespace App\Workers\Executers;

use AmoCRM\Client\AmoCRMApiClient;
use App\Workers\Model\Beanstalk;
use Laminas\ServiceManager\Factory\FactoryInterface;
use \Interop\Container\ContainerInterface;
use MailchimpMarketing\ApiClient;

class AccountSyncWorkerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        return new AccountSyncWorker(
            $container->get(Beanstalk::class),
            $container->get(AmoCRMApiClient::class),
            $container->get(ApiClient::class)
        );
    }
}
