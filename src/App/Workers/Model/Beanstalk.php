<?php

declare(strict_types=1);

namespace App\Workers\Model;

use Pheanstalk\Pheanstalk;
use App\Workers\Config\BeanstalkConfig;

class Beanstalk
{
    protected BeanstalkConfig $config;
    protected ?Pheanstalk $connect = null;


    public function __construct(BeanstalkConfig $config)
    {
        $this->config = $config;
    }

    public function send(string $queueName, $data): void
    {
        $connect = $this->getConnect();

        $connect->useTube($queueName);
        $connect->put(json_encode($data), JSON_THROW_ON_ERROR);
    }

    public function getConnect(): Pheanstalk
    {
        if ($this->connect === null) {
            $this->connect = Pheanstalk::create(
                $this->config->getHost(),
                $this->config->getPort(),
                $this->config->getTimeout(),
            );
        }

        return $this->connect;
    }

}
