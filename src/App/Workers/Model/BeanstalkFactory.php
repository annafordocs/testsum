<?php


namespace App\Workers\Model;
use App\Workers\Config\BeanstalkConfig;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;


class BeanstalkFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {

        return new Beanstalk($container->get(BeanstalkConfig::class));
    }
}
