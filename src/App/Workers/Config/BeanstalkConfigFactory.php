<?php

declare(strict_types=1);

namespace App\Workers\Config;

use Laminas\ServiceManager\Factory\FactoryInterface;
use \Interop\Container\ContainerInterface;


class BeanstalkConfigFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): BeanstalkConfig
    {
        $config = $container->get('config')['beanstalk'];

        return new BeanstalkConfig(
            $config['host'],
            $config['port'],
            $config['timeout']
        );
    }

}
