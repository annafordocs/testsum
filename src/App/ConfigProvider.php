<?php

declare(strict_types=1);

namespace App;

use AmoCRM\Client\AmoCRMApiClient;
use App\Handler\AmoClientFactory;
use App\Handler\MailchimpApiClientFactory;
use App\Handler\SumHandler;
use App\Handler\SumHandlerFactory;
use App\Handler\WebhookHandler;
use App\Handler\WebhookHandlerFactory;
use App\Workers\Config\BeanstalkConfig;
use App\Workers\Config\BeanstalkConfigFactory;
use App\Workers\Executers\AccountSyncWorker;
use App\Workers\Executers\AccountSyncWorkerFactory;
use App\Workers\Model\Beanstalk;
use App\Workers\Model\BeanstalkFactory;
use MailchimpMarketing\ApiClient;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.laminas.dev/laminas-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
            'laminas-cli' => $this->getCliConfig(),
        ];
    }

    public function getCliConfig() : array
    {
        return [
            'commands' => [
                'crm:worker:account_sync' => AccountSyncWorker::class,
            ],
        ];
    }


    /**
     * Returns the container dependencies
     */
    public function getDependencies(): array
    {
        return [
            'invokables' => [
                Handler\PingHandler::class => Handler\PingHandler::class,
            ],
            'factories'  => [
                Handler\HomePageHandler::class => Handler\HomePageHandlerFactory::class,
                Handler\AuthHandler::class => Handler\AuthHandlerFactory::class,
                AmoCRMApiClient::class => AmoClientFactory::class,
                BeanstalkConfig::class => BeanstalkConfigFactory::class,
                AccountSyncWorker::class => AccountSyncWorkerFactory::class,
                Beanstalk::class => BeanstalkFactory::class,
                SumHandler::class => SumHandlerFactory::class,
                ApiClient::class => MailchimpApiClientFactory::class,
                WebhookHandler::class => WebhookHandlerFactory::class,


            ],
        ];
    }

    /**
     * Returns the templates configuration
     */
    public function getTemplates(): array
    {
        return [
            'paths' => [
                'app'    => ['templates/app'],
                'error'  => ['templates/error'],
                'layout' => ['templates/layout'],
            ],
        ];
    }
}
