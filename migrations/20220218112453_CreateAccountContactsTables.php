<?php

use Phpmig\Migration\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccountContactsTables extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();
        $db = $container['db'];

        $db::schema()->create(
            'account_contacts',
            function (Blueprint $table) {
                $table->id();
                $table->string('name', 255)->nullable();
                $table->string('email', 255)->unique()->nullable();
                $table->biginteger('amocrm_account_id');
                $table->timestamps();
                $table->softDeletes($column = 'deleted_at');
            }
        );
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $container = $this->getContainer();
        $db = $container['db'];

        $db::schema()->dropIfExists('account_contacts');
    }
}
