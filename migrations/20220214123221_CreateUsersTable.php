<?php

declare(strict_types=1);

use Phpmig\Migration\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();
        $db = $container['db'];

        $db::schema()->create('users', function (Blueprint $table) {
            $table->id();
            $table->biginteger('amocrm_account_id')->unique();
            $table->string('accessToken', 1000)->unique()->nullable();
            $table->string('refreshToken', 1000)->unique()->nullable();
            $table->string('baseDomain', 1000)->unique();
            $table->biginteger('mailchimp_account_id')->nullable();
            $table->bigInteger('expires')->nullable();
            $table->timestamps();
            $table->softDeletes($column = 'deleted_at');

        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $container = $this->getContainer();
        $db = $container['db'];

        $db::schema()->dropIfExists('users');
    }
}
